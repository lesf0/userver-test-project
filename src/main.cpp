#include <userver/components/minimal_server_component_list.hpp>
#include <userver/utils/daemon_run.hpp>
#include <userver/clients/dns/component.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/testsuite/testsuite_support.hpp>

#include <telegram/api/webhook.hpp>
#include <telegram/processors/store_photo.hpp>
#include <telegram/processors/search_photo.hpp>
#include <component_names.hpp>

int main(int argc, char* argv[]) {
  const auto component_list =
      userver::components::MinimalServerComponentList()
          .Append<telegram::WebhookApi>()
          .Append<telegram::StorePhotoProcessor>()
          .Append<telegram::SearchPhotoProcessor>()
          .Append<userver::components::Postgres>(kDbName)
          .Append<userver::components::TestsuiteSupport>()
          .Append<userver::clients::dns::Component>();
  return userver::utils::DaemonMain(argc, argv, component_list);
}
/// [Postgres service sample - main]