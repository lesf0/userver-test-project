#pragma once

#include <optional>

#include <userver/formats/json/value.hpp>
#include <userver/engine/shared_mutex.hpp>

namespace telegram {

class AbstractApi {
  public:
    static constexpr std::string_view kName = "telegram-api";

    using MessageProcessor = std::function<std::optional<userver::formats::json::Value>(const userver::formats::json::Value&)>;

    void RegisterMessageProcessor(const std::string& name, MessageProcessor&& message_processor);
    void UnregisterMessageProcessor(const std::string& name);

  protected:
    AbstractApi() = default;

    std::optional<userver::formats::json::Value> ProcessMessage(const userver::formats::json::Value& message) const;
  private:
    std::unordered_map<std::string, MessageProcessor> message_processors_;
    mutable userver::engine::SharedMutex message_processors_mutex_;
};

}; // namespace telegram