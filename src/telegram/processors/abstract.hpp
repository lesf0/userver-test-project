#pragma once

#include <userver/components/loggable_component_base.hpp>

#include <telegram/api/abstract.hpp>

namespace telegram {

class AbstractProcessor : public userver::components::LoggableComponentBase {
  protected:
    AbstractProcessor(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context,
      const std::string_view& name);
    ~AbstractProcessor();

    virtual std::optional<userver::formats::json::Value> ProcessMessage(const userver::formats::json::Value&) const = 0;

  private:
    AbstractApi& api_;
    const std::string name_;
};

}; // namespace telegram