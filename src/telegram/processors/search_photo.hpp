#pragma once

#include "abstract.hpp"

#include <userver/storages/postgres/postgres_fwd.hpp>

namespace telegram {

class SearchPhotoProcessor : public AbstractProcessor {
  public:
    static constexpr std::string_view kName = "search-photo-processor";

    SearchPhotoProcessor(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context);

  protected:
    virtual std::optional<userver::formats::json::Value> ProcessMessage(const userver::formats::json::Value&) const override;

  private:
    userver::storages::postgres::ClusterPtr pg_cluster_;
};

}; // namespace telegram