#include "search_photo.hpp"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <userver/components/component_context.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/formats/parse/common_containers.hpp>

#include <component_names.hpp>

namespace telegram {

namespace {

const userver::storages::postgres::Query kSearchPhoto{
    "SELECT uuid_id, file_id FROM stored_photos "
    "WHERE (caption <->> $1) < 0.9 "
    "ORDER BY (caption <->> $1) ASC "
    "OFFSET $2 LIMIT $3",
    userver::storages::postgres::Query::Name{"search_photo"},
};

struct PgFoundPhoto {
    boost::uuids::uuid uuid_id;
    std::string file_id;
};

const int64_t kLimit = 10;

} // namespace

SearchPhotoProcessor::SearchPhotoProcessor(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context)
    : AbstractProcessor(config, context, kName)
    , pg_cluster_{
          context.FindComponent<userver::components::Postgres>(kDbName)
              .GetCluster()}
{
}

std::optional<userver::formats::json::Value> SearchPhotoProcessor::ProcessMessage(const userver::formats::json::Value& data) const {
    try {
        const auto& inline_query = data["inline_query"];
        const auto query = inline_query["query"].As<std::string>();
        const auto offset_str = inline_query["offset"].As<std::string>();

        int64_t offset = 0;
        if (!offset_str.empty()) {
            offset = std::stoi(offset_str);
        }
        
        auto file_infos = pg_cluster_->Execute(userver::storages::postgres::ClusterHostType::kSlave,
                                    kSearchPhoto, query, offset, kLimit + 1)
                                .AsContainer<std::vector<PgFoundPhoto>>(userver::storages::postgres::kRowTag);

        bool has_more = file_infos.size() > kLimit;

        userver::formats::json::ValueBuilder response_builder;
        response_builder["method"] = "answerInlineQuery";
        response_builder["inline_query_id"] = inline_query["id"];
        if (has_more) {
            response_builder["next_offset"] = std::to_string(offset + kLimit);
        }
        auto response_array = response_builder["results"];
        for (auto&& file_info : file_infos) {
            userver::formats::json::ValueBuilder item_builder;
            item_builder["type"] = "photo";
            item_builder["id"] = boost::uuids::to_string(file_info.uuid_id);
            item_builder["photo_file_id"] = std::move(file_info.file_id);
            response_array.PushBack(std::move(item_builder));
        }
        return response_builder.ExtractValue();
    } catch (const std::exception&) {
        return std::nullopt;
    }
}

}; // namespace telegram