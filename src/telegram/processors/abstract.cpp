#include "abstract.hpp"

#include <userver/components/component_context.hpp>

namespace telegram {

AbstractProcessor::AbstractProcessor(
  const userver::components::ComponentConfig& config,
  const userver::components::ComponentContext& context,
  const std::string_view& name)
  : LoggableComponentBase(config, context)
  , api_{context.FindComponent<AbstractApi>()}
  , name_{name}
{
    using namespace std::placeholders;

    api_.RegisterMessageProcessor(name_, std::bind(&AbstractProcessor::ProcessMessage, this, _1));
}

AbstractProcessor::~AbstractProcessor() {
    api_.UnregisterMessageProcessor(name_);
}

}; // namespace telegram