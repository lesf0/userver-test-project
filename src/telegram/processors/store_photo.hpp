#pragma once

#include "abstract.hpp"

#include <userver/storages/postgres/postgres_fwd.hpp>

namespace telegram {

class StorePhotoProcessor : public AbstractProcessor {
  public:
    static constexpr std::string_view kName = "store-photo-processor";

    StorePhotoProcessor(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context);

  protected:
    virtual std::optional<userver::formats::json::Value> ProcessMessage(const userver::formats::json::Value&) const override;

  private:
    userver::storages::postgres::ClusterPtr pg_cluster_;
};

}; // namespace telegram