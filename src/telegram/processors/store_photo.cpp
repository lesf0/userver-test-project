#include "store_photo.hpp"

#include <userver/components/component_context.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/formats/parse/common_containers.hpp>

#include <component_names.hpp>

namespace telegram {

namespace {

const userver::storages::postgres::Query kStorePhoto{
    "INSERT INTO stored_photos (file_id, caption) "
    "VALUES ($1, $2)",
    userver::storages::postgres::Query::Name{"store_photo"},
};

} // namespace

StorePhotoProcessor::StorePhotoProcessor(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context)
    : AbstractProcessor(config, context, kName)
    , pg_cluster_{
          context.FindComponent<userver::components::Postgres>(kDbName)
              .GetCluster()}
{
}

std::optional<userver::formats::json::Value> StorePhotoProcessor::ProcessMessage(const userver::formats::json::Value& data) const {
    try {
        const auto& message = data["message"];
        const auto& chat = message["chat"];

        if (chat["type"].As<std::string>() != "private") {
            return std::nullopt;
        }

        userver::formats::json::ValueBuilder response_builder;
        response_builder["method"] = "sendMessage";
        response_builder["chat_id"] = chat["id"];

        const auto file_id = message["photo"].rbegin()->operator[]("file_id").As<std::string>();
        const auto caption = message["caption"].As<std::optional<std::string>>();

        if (!caption) {
            response_builder["text"] = "Please provide a caption to your photo";
            return response_builder.ExtractValue();
        }

        auto res =
            pg_cluster_->Execute(userver::storages::postgres::ClusterHostType::kMaster,
                                 kStorePhoto, file_id, *caption);

        if (res.RowsAffected()) {
            response_builder["text"] = "Successfully stored";
        } else {
            response_builder["text"] = "Postgresql error";
        }

        return response_builder.ExtractValue();
    } catch (const std::exception&) {
        return std::nullopt;
    }
}

}; // namespace telegram