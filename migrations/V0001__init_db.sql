CREATE TABLE stored_photos (
    id          BIGSERIAL   NOT NULL,
    file_id     TEXT        NOT NULL,
    caption     TEXT        NOT NULL
);

CREATE INDEX stored_photos_caption_gin_trgm_idx ON stored_photos USING gin (caption gin_trgm_ops);