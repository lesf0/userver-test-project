cmake_minimum_required(VERSION 3.19)

project(userver-hello CXX)

add_executable(
    ${PROJECT_NAME}
    "src/main.cpp"

    "src/telegram/api/abstract.cpp"
    "src/telegram/api/webhook.cpp"
    "src/telegram/processors/abstract.cpp"
    "src/telegram/processors/store_photo.cpp"
    "src/telegram/processors/search_photo.cpp"
)
target_link_libraries(${PROJECT_NAME} userver-core userver-postgresql)
set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)
include_directories("src")

add_subdirectory(submodules/userver)
